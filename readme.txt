%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ETHZ RueMonge 2014 dataset + load/ evaluation code
%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ECCV 2014, Zurich, Switzerland. (PDF, poster, project)
% H. Riemenschneider, A. Bodis-Szomoru, J. Weissenberg, L. Van Gool
%
% http://www.varcity.eu
% http://varcity.eu/3dchallenge/
% http://www.vision.ee.ethz.ch/~rhayko/paper/eccv2014_riemenschneider_multiviewsemseg/
%
% Cite the above work if you use any of the code or dataset.
% Please submit your results to the VarCity3D Challenge leadership board - http://goo.gl/forms/AbbYzw7WZq
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OVERVIEW
%
% EVALUATION
% TASK 1 - Image Labelling - vanilla 2d img labelling task (NEW TASK && INCLUDED CODE)
% TASK 2 - Mesh Labelling - collect or reason in 3d to label mesh (ECCV paper INCLUDED CODE)
% TASK 3 - Pointcloud Labelling - collect or reason in 3d to label point cloud (NEW TASK && CVPR15 paper)
% TASK 4 - View selection - reason which images to skip for features & classification via view reduction (ECCV14 paper)
% TASK 5 - Scene Coverage - reason which images to skip for features & classification via scene coverage (ECCV14 paper)
%
% PROTOCOL evaluation: report average / individual pascal scores (score.mean_pascal)
% PROTOCOL ECCV PAPER   :  train set 119 images, test set 196 images, evaluate on 3d mesh directly (15.5 buildings)
% PROTOCOL NEW THIS CODE:  train set 113 images, test set 202 images, evaluate on 3d mesh directly (16 full buildings)
%                          test set uses all 202 images (not just 106 labelled) (IMG_5504 till IMG_5705, all images additional to folder test)
%
%
% DATA FORMAT:
% data/image        - raw rgb images as jpg
% data/index        - triangle index from image to mesh
% data/label        - GT labels for training / testing images
% data/list_full    - evaluation protocol - split into test/train (do your own val)
% data/mesh.ply     - surface mesh used for 3d representation (eccv14 paper)
% data/pcl.ply      - pointcloud used for 3d representation (cvp15 paper)
%
% data/eccv2014/imageprob2mesh/    - eccv14 raw probabilities
% data/eccv2014/imagemap2mesh/     - eccv14 MAP label maps
% data/eccv2014/imagegco2mesh/     - eccv14 GCO label maps
%
% listall.txt       - all 428 images
% listeval.txt      - 202 testing images
% listtrain.txt     - 113 training images
%
% data/mesh_gt_train.ply     - data and GT for training
% data/mesh_gt_test.ply      - data and GT for testing


% CODE OVERVIEW
%  
% EVALUATION    - evaluation_multilabel.m, evaluation_multilabel_print.m
% COLLECTION 3D - mesh_collect_files.m, mesh_collect_raw.m, mesh_collect_labelmap.m
% GRAPHCUT 3D   - mesh_adjacency_onehit.m, conversion_likelihood2cost.m, ICG_Graphcut_GCO.m
% MISC          - progressbartime.m, rgb2labelmap.m, export_ply_simple.m
%
% REQUIREMENTS:
%   GCO graphcut library from http://vision.csd.uwo.ca/code/gco-v3.0.zip (see thirdparty)
%
% CHANGES
% v1.2  - small bug fixed (renaming, etc)
% v1.1  - reimplementation of image2mesh, mesh2mesh for easier task use
%       - implementation of gmm for mesh2mesh example
%       - single matrix for raw probabilities for all views (only 13 gbyte now)
%       - implementation of fusion baselines (sum, mean, max, etc)
%       - implementation of confusion table visualization

% v1.0  - reimplementation of loading and evaluation codes for public
%       - changes in training / testing split (old covered 15.5 vs. new 16 buildings).
%       - bug fix in gt (smoothing, missed some triangles)
%
% AUTHOR: Hayko Riemenschneider, 2014-2015
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% TASK 1 - Image Labelling - vanilla 2d img labelling task

task1_image

%% TASK 2 - Mesh Labelling - collect or reason in 3d to label mesh
% SUMMARY

% THREE MODES
% 2a - Image2Mesh Labelling - collect images to label mesh (THIS CODE)
% 2b - Pointcloud2Mesh Labelling - collect pointcloud to label mesh
% 2c - Mesh2Mesh Labelling - collect mesh results directy

task2a_image2mesh
task2b_pcl2mesh
task2c_mesh2mesh


% YOUR OWN TRAINING OUTSIDE?
%   clone 'results_eccv2014' and 'imagemap2mesh' / 'imagegco2mesh' folders
%   produce .mat or .png files and use code to load in 'results_yours'
%   produce data.predictprob [202 x 8 x numFaces]
%   select fusion method

% YOUR OWN TRAINING HERE?
%   see examples in task2c_mesh2mesh.m
%
% YOUR OWN FUSION?
%   load('imageprob2mesh_data.mat','data')
%
% YOUR OWN 3D MESH LABELING?
%   dataset.mesh_predict == [1 x num_face] labels ranging from [0...num_label]
%   score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset_labelskip)



References:
[1] Learning Where To Classify In Multi-View Semantic Segmentation
    H. Riemenschneider, A. Bodis-Szomoru, J. Weissenberg, L. Van Gool, ECCV 2014

[2] 3D All The Way: Semantic Segmentation of Urban Scenes From Start to End in 3D
	A. Martinovic, J. Knopp, H. Riemenschneider, L. Van Gool, CVPR 2015

[3] VarCity3D Challenge leadership board - http://goo.gl/forms/AbbYzw7WZq


Happy 3D Semantics! :)