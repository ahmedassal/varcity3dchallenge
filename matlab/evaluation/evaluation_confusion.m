function normconfusion = evaluation_confusion(confusion, labelText, titleText)
% normconfustion = evaluation_confusion(confusion, labelText, titleText)
% fancy confusion table visualizer
% author: hayko riemenschneider, 2015

% demo (run without parameters)
if(~exist('confusion','var'))
    confusion = rand(5);
    labelText = {'a','b','c','d','e'};
    titleText = 'random';
end

numClasses = size(confusion, 1);
% normalize confusion matrix
for i=1:numClasses
    x = confusion(i,:)./sum(confusion(i,:));
    normconfusion(i,:) = x';
end


% set heatmap and title
figure,
imagesc(1-normconfusion)
axis square
colormap('Gray')
colormap('Hot')

if(exist('titleText','var'))
title([titleText, sprintf(' mean=%0.2f',mean(diag(normconfusion)))]) % only mean recall
title([titleText])
end
% set labels
set(gca,'xtick', 1:numClasses);
set(gca,'ytick', 1:numClasses);

if(exist('labelText','var'))
set(gca,'xticklabel', cell(labelText));
set(gca,'yticklabel', cell(labelText));
end

set(gca,'FontWeight', 'bold');
%set(gca,'FontSize', 7);

% place text inside the confusion table
for i=1:numClasses
    for j=1:numClasses

        % inverse color for very dark backgrounds (e.g. high scores)
        if normconfusion(i,j) > 0.3
            textColor = 'white';
        else
            textColor = 'black';
        end
        
        text(j, i, sprintf('%0.2f', normconfusion(i,j)), 'Color', textColor, 'FontWeight', 'bold', ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle');
        
    end
end

