function list_idx = load_listindex(filename)
% list_idx = load_listindex(filename)
%   load filename list index for train or test set
%
% author: hayko riemenschneider, 2014-2015
%

fid = fopen(filename);
    list_idx = textscan(fid, '%s'); fclose(fid);
    list_idx = list_idx{1};
    display(['found ' num2str(length(list_idx)) ' files in ' filename])
end