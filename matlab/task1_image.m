% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ETHZ RueMonge 2014 dataset + load / evaluation code
%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ECCV 2014, Zurich, Switzerland. (PDF, poster, project)
% H. Riemenschneider, A. Bodis-Szomoru, J. Weissenberg, L. Van Gool
%
% http://varcity.eu/3dchallenge/
% http://www.vision.ee.ethz.ch/~rhayko/paper/eccv2014_riemenschneider_multiviewsemseg/
%
% Please cite the above work if you use any of the code or dataset.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TASK 1 - Image Labelling - vanilla 2d img labelling task

close all
clear 
clc

%% SETUP PATHS
addpath(genpath('.'))

%method = 'imageprob2image'; % direct PROBABILITY image evaluation - needs your own graphoptimization
method = 'imagegco2image'; % direct MAP image evaluation
method = 'imagemap2image'; % direct GCO image evaluation

dataset = load_dataset('ruemonge428','results_eccv2014', method);

       
%% EVALUATION TASK 1 - Image Labelling - vanilla 2d img labelling task (THIS CODE)
display(['evaluating results...'])

% DATA COLLECTION 2D LABELS
[gt, res] = evaluation_load_folder (dataset.test.gt, dataset.predicttest, dataset.test.list_idx, dataset.test.num_view, dataset.cm);
score = evaluation_multilabel(gt, res, dataset.labelskipidx, dataset.num_label)
evaluation_confusion(score.confusion, dataset.labels_str_used, [dataset.name ' ' method]);
saveas(gcf,[dataset.predicttestfilescore(1:end-4) '.png'])

% DATA COLLECTIOn 2D PROB (generate MAP and GCO)
% try your own :)


display(['done!'])
diary off;
